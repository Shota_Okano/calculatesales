package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String SALES_FILE_INVALID_NAME = "売上ファイル名が連番になっていません";
	private static final String SALES_AMOUNT_OVER_TEN_DIGITS = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param "C:\Users\okano.shota\Desktop\first-repository\Java 売上集計S" - コマンドライン引数
	 * @param rcdFiles - 売上ファイルのリスト
	 */
	public static void main(String[] args) {
		BufferedReader br = null;
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理 + 商品定義ファイル読み込み処理
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
		}
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店", "[0-9]{3}")) {
			return;
		}
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "商品", "[0-9a-zA-Z]{8}")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(File file : files) {
			if(file.isFile() && file.getName().matches("^[0-9]{8}.rcd$")) { //  ^[0-9]{8} + \\.rcd$
				rcdFiles.add(file);
			}
		}

		// エラー処理[2-1-1]:ファイル名(連番)確認
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if(latter - former != 1) {
				System.out.println(SALES_FILE_INVALID_NAME);
				return;
			}
		}

		for(File rcdFile : rcdFiles) {
			try {
				String line;
				FileReader fr = new FileReader(rcdFile);
				br = new BufferedReader(fr);

				// 各rcdFileの中身をListに追加 + エラー処理[2-3]：売上ファイル桁数確認
				List<String> rcdFileDatas = new ArrayList<>();

				while((line = br.readLine()) != null) {
					rcdFileDatas.add(line);
				}

				// .rcd 行数３行を確認
				if(rcdFileDatas.size() != 3) {
					System.out.println(rcdFile.getName() + "のフォーマットが不正です");
				}

				// エラー処理[2-2]：支店コード確認
				String branchCode = rcdFileDatas.get(0);
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFile.getName() + "の支店コードが不正です");
					return;
				}
				// 追加エラー処理：商品コード確認
				String commodityCode = rcdFileDatas.get(1);
				if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFile.getName() + "の商品コードが不正です");
				}

				// ３行目売上金額が正しい数値か確認し、各マップへ値を保持させる。
				if(!rcdFileDatas.get(2).matches("^[0-9]+$")) {              // エラー処理[3-3]
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				Long fileSale = Long.parseLong(rcdFileDatas.get(2));
				Long salesAmount = branchSales.get(branchCode) + fileSale;
				Long commoditySalesAmount = commoditySales.get(commodityCode) + fileSale;

				// エラー処理[2-1]：売上合計金額が10桁以下であることを確認
				if(salesAmount >= 1000000000L || commoditySalesAmount >= 1000000000L) {
					System.out.println(SALES_AMOUNT_OVER_TEN_DIGITS);
					return;
				}

				// 各出力用HashMapに合計金額を挿入。
				branchSales.replace(branchCode, salesAmount);
				commoditySales.replace(commodityCode, commoditySalesAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル ＋ 商品別集計ファイル：書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}


	}

	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	/**
	 * 支店定義ファイル + 商品定義ファイル読み込み処理
	 *
	 * @param path - フォルダパス
	 * @param fileName - 支店定義ファイル名
	 * @param hashMapNames - 支店・商品コードと支店名を保持するMap
	 * @param hashMapSales - 支店・商品コードと売上金額を保持するMap
	 * @param category - 支店 or 商品
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> hashMapNames, Map<String, Long> hashMapSales, String category, String regex) {
		BufferedReader br = null;
		String line;

		try {
			File file = new File(path, fileName);
			if(!file.exists()) {                                    // エラー処理[1-1]
				System.out.println(category + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				if((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(category + FILE_INVALID_FORMAT);
				}

				hashMapNames.put(items[0], items[1]);
				hashMapSales.put(items[0], 0L);

				// branchSales.get(items[0]); -> null
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param path - フォルダパス
	 * @param fileName - 支店定義ファイル名
	 * @param hashMapNames - 支店・商品コードと支店名を保持するMap
	 * @param hashMapSales - 支店・商品コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> hashMapNames, Map<String, Long> hashMapSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		File file;

		try {
			file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : hashMapNames.keySet()) {
				bw.write(key + "," + hashMapNames.get(key) + "," + hashMapSales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
